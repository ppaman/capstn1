﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class Fuel : MonoBehaviour
{

    public Canvas canvas;
    public Slider slider;
    public Button button;
    float currentFuel;
    bool isFueling = false;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("FuelCanvas").GetComponent<Canvas>();
        slider = GameObject.FindGameObjectWithTag("FuelCanvas").GetComponentInChildren<Slider>();
        button = GameObject.FindGameObjectWithTag("FuelCanvas").GetComponentInChildren<Button>();
        currentFuel = slider.minValue;

        EventTrigger trigger = button.gameObject.AddComponent<EventTrigger>();
        var pointerDown = new EventTrigger.Entry();
        var pointerUp = new EventTrigger.Entry();
        pointerDown.eventID = EventTriggerType.PointerDown;
        pointerUp.eventID = EventTriggerType.PointerUp;
        pointerDown.callback.AddListener((e) => OnPress());
        pointerUp.callback.AddListener((e) => OnRelease());
        trigger.triggers.Add(pointerDown);
        trigger.triggers.Add(pointerUp);
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = currentFuel;

        if(isFueling)
        {
            AddFuel();
        }

        if(currentFuel >= slider.maxValue)
        {
            OnComplete();
        }
    }

    public void AddFuel()
    {
        currentFuel++;
    }

    public void OnPress()
    {
        isFueling = true;
    }

    public void OnRelease()
    {
        isFueling = false;
    }

    public void OnComplete()
    {
        Destroy(canvas.gameObject);
        Destroy(gameObject);
    }
}
