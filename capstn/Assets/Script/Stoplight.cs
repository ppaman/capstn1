﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stoplight : MonoBehaviour
{
    public GameObject GreenLight;
    public GameObject RedLight;
    public float timer;

    private float initialTime;
    private void Start()
    {
        initialTime = timer;
        RedLight.SetActive(false);
        GreenLight.SetActive(true);
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if(timer<=0)
        {
            ChangeLights();
            timer = initialTime;
        }
    }

    void ChangeLights()
    {
       if(GreenLight.activeSelf==true)
        {
            RedLight.SetActive(true);
            GreenLight.SetActive(false);
        }
       else if(RedLight.activeSelf==true)
        {
            GreenLight.SetActive(true);
            RedLight.SetActive(false);
        }
    }
}
