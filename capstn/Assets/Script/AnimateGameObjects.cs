﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateGameObjects : MonoBehaviour
{
    public float speed;
    public float length;
    private Vector2 startPos;

    private void Start()
    {
        startPos = this.transform.position;
    }

    void Update()
    {
        DropoffAnimation();
    }

    void DropoffAnimation()
    {
        float y = startPos.y + Mathf.PingPong(Time.time * speed, length);
        Vector3 pos = new Vector3(this.transform.position.x, y, this.transform.position.z);
        this.transform.position = pos;
    }
}
