﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAI : MonoBehaviour
{
    public GameObject[] waypoints;

    [SerializeField]
    float moveSpeed = 2f;

    [SerializeField]
    GameObject offsetLeft;

    [SerializeField]
    GameObject offsetRight;

    [SerializeField]
    GameObject HazardSpawn;

    int waypointIndex = 0;
    float startingSpeed;
    public float raycastMaxDistance = 1f;
    public bool isHazardOn;
    public GameObject HazardObject;
    public float HazardSpawnChance;
    public float HazardSpawnInterval;

    RaycastHit2D hit;
    RaycastHit2D hitLeft;
    RaycastHit2D hitRight;

    private void Awake()
    {
        //Physics2D.autoSyncTransforms = true;
    }
    void Start()
    {
        Physics2D.queriesStartInColliders = false;
        startingSpeed = moveSpeed;
        isHazardOn = false;

        StartCoroutine(CheckForHazard());
    }

    void Update()
    {
        Move();

        if (isHazardOn == false)
        {
            if (hit.collider == null || hit.collider.tag == "GreenLight" || hitLeft.collider == null || hitLeft.collider.tag == "GreenLight" || hitRight.collider == null || hitRight.collider.tag == "GreenLight")
            {
                moveSpeed = startingSpeed;
            }
            else if (hit.collider.tag == "car" || hit.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitLeft.collider.tag == "car" || hitLeft.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitRight.collider.tag == "car" || hitRight.collider.tag == "RedLight" || hit.collider.tag == "Player")
            {
                moveSpeed = 0f;
            }
        }
    }

    private void FixedUpdate()
    {
        
    }

    private void LateUpdate()
    {
        CheckCollision();
    }

    void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);
        if (transform.position == waypoints[waypointIndex].transform.position)
        {
            if (waypointIndex < waypoints.Length - 1)
                waypointIndex += 1;
        }

        Vector3 vectorToTarget = waypoints[waypointIndex].transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 10f);

        Physics2D.SyncTransforms();
    }


    void CheckCollision()
    {
        int carLayerMask = 1 << LayerMask.NameToLayer("Car");
        int stopLightLayerMask = 1 << LayerMask.NameToLayer("Stoplight");

        hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.right), raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.red);

        hitLeft = Physics2D.Raycast(offsetLeft.transform.position, transform.TransformDirection(Vector2.right) , raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(offsetLeft.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);

        hitRight = Physics2D.Raycast(offsetRight.transform.position, transform.TransformDirection(Vector2.right) , raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(offsetRight.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);
    }

    //public void OnHazardClick()
    //{
    //    isHazardOn = false;
    //}

    void SpawnHazardPin()
    {
        isHazardOn = true;
        moveSpeed = 0f;

        GameObject hazard = Instantiate(HazardObject, HazardSpawn.transform.position, Quaternion.identity);
        hazard.GetComponent<PickupButtonScript>().AiParent = this;
    }

    //void SpawnMiniGame()
    //{
    //    GameObject miniGame = Instantiate(MiniGame, transform.position, Quaternion.identity);
    //    Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    //    miniGame.transform.SetParent(canvas.transform, false);
    //}

    IEnumerator CheckForHazard()
    {
        yield return new WaitForSeconds(HazardSpawnInterval);

        if (isHazardOn == false)
        {
            int RandomInt = Random.Range(0, 100);
            if (RandomInt <= HazardSpawnChance)
            {
                SpawnHazardPin();
            }
            StartCoroutine(CheckForHazard());
        }
        else
        {
            StartCoroutine(CheckForHazard());
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("HazardChecker"))
        {
            isHazardOn = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("HazardChecker"))
        {
            isHazardOn = false;
        }
    }

}
