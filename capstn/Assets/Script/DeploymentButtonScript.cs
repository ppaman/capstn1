﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class DeploymentButtonScript : MonoBehaviour
{
    public Button redButton;
    public Button blueButton;
    public Text cooldownText;
    public Text readyText;
    public float cooldownTime;
    private float currentCooldownTime;

    private void Start()
    {
        //GameObject.Find("DeploymentPanel").transform.localScale = new Vector3(0, 0, 0);

        readyText.enabled = true;
        cooldownText.enabled = false;
        currentCooldownTime = cooldownTime;
    }

    public void StartCooldown()
    {
        StartCoroutine(Cooldown());
    }

    IEnumerator Cooldown()
    {
        readyText.enabled = false;
        cooldownText.enabled = true;
        float counter = currentCooldownTime;
        cooldownText.text = counter.ToString();
        while (counter > 0)
        {
            redButton.interactable = false;
            blueButton.interactable = false;

            yield return new WaitForSeconds(1);
            counter --;
            cooldownText.text = counter.ToString();
        }
        readyText.enabled = true;
        cooldownText.enabled = false;
        redButton.interactable = true;
        blueButton.interactable = true;
    }

    public void HideDeploymentTab()
    {
        GameObject.Find("DeploymentPanel").transform.localScale = new Vector3(0, 0, 0);
    }

    public void ShowDeploymentTab()
    {
        GameObject.Find("DeploymentPanel").transform.localScale = new Vector3(1, 1, 1);
    }    
}
