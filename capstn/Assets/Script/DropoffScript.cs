﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropoffScript : MonoBehaviour
{
    public int DropoffPoints;

    public bool isRedDropoff;
    public bool isBlueDropoff;
    public bool isGreenDropoff;
    public GameObject PointPrefabSpawn;
    public GameObject PointsPrefab;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (isRedDropoff == true && collision.gameObject.GetComponent<DeployPlayer>().isRed == true || isBlueDropoff == true && collision.gameObject.GetComponent<DeployPlayer>().isBlue == true || isGreenDropoff == true && collision.gameObject.GetComponent<DeployPlayer>().isGreen == true)
            {
                collision.gameObject.GetComponent<DeployPlayer>().DropoffSequence();
                //Dropoff(collision.gameObject.GetComponent<DeployPlayer>().waitTime);
            }   
        }
    }

    IEnumerator DropOffTime(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject point = Instantiate(PointsPrefab, PointPrefabSpawn.transform.position, Quaternion.identity);
        point.GetComponent<AnimatePoints>().SetColor(Color.white);
        point.GetComponent<AnimatePoints>().SetText("+" + DropoffPoints.ToString());
        GameManager.instance.AddScore(DropoffPoints);
    }

    void Dropoff(float waitTime)
    {
        StartCoroutine(DropOffTime(waitTime));
    }
}
