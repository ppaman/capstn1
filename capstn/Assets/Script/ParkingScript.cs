﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkingScript : MonoBehaviour
{
    public int DropoffPenalty;
    public GameObject PointsPrefab;
    public GameObject PointPrefabSpawn;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("PlayerDespawned");
            GameManager.instance.SubtractFillAmount(1f);
            Destroy(collision.gameObject);
            foreach (SpawnerScript spawner in GameManager.instance.Spawners)
            {
                spawner.DecreaseTraffic();
            }
        }
        else if (collision.gameObject.tag == "car")
        {
            if(PointPrefabSpawn != null && PointsPrefab != null)
            {
                GameObject point = Instantiate(PointsPrefab, PointPrefabSpawn.transform.position, Quaternion.identity);
                point.GetComponent<AnimatePoints>().SetColor(Color.red);
                point.GetComponent<AnimatePoints>().SetText("-" + DropoffPenalty.ToString());
                GameManager.instance.SubtractScore(DropoffPenalty);
                GameManager.instance.SubtractFillAmount(1f);
                Destroy(collision.gameObject);
                foreach(SpawnerScript spawner in GameManager.instance.Spawners)
                {
                    spawner.IncreaseTraffic();
                }
            }
        }
    }
}
