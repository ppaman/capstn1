﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public Image BarImage;
    public Text ScoreText;
   [SerializeField] int Score;

    public DeployPlayer CarWithMiniGame;
    public CarAI AIWithMiniGame;
    public bool isMiniGameActive;
    public SpawnerScript[] Spawners;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        float height = Camera.main.orthographicSize * 2;
        float width = height * Screen.width / Screen.height;

        isMiniGameActive = false;
        ScoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        Score = 0;
        ScoreText.text = Score.ToString();
        BarImage.fillAmount = 0;
    }

    public void AddScore(int points)
    {
        Score += points;
        ScoreText.text = Score.ToString();
    }

    public void SubtractScore(int numToSubtract)
    {
        Score -= numToSubtract;
        ScoreText.text = Score.ToString();
    }

    public void SetCarWithMinigame(DeployPlayer player)
    {
        CarWithMiniGame = player;
    }

    public void EndMiniGame()
    {
        if(CarWithMiniGame!= null)
        {
            CarWithMiniGame.SpawnPointPrefab();
            CarWithMiniGame.isPickingUp = false;
            //CarWithMiniGame.moveSpeed = 2f;
            CarWithMiniGame = null;
            isMiniGameActive = false;
        }
        else if(AIWithMiniGame != null)
        {
            AIWithMiniGame.isHazardOn = false;
            AIWithMiniGame = null;
        }
    }

    public void AddBarFillAmount(float amountToAdd)
    {
        BarImage.fillAmount += ((amountToAdd/100) *2);
    }

    public void SubtractFillAmount(float amountToSubtract)
    {
        BarImage.fillAmount -= ((amountToSubtract / 100) * 2);
    }
}
