﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DeploymentScript : MonoBehaviour
{
    [SerializeField] UnityEvent anEvent;

    public GameObject SpawnPoint;
    //public GameObject[] Players;

    public GameObject BluePlayer;
    public GameObject RedPlayer;

    public GameObject[] BluePathList;
    public GameObject[] RedPathList;

    public Text[] PathTexts;

    public Button[] PathButtons;
    public Button DeployButton;

    [SerializeField] bool isRed;
    [SerializeField] bool isBlue;

    private GameObject[] pathToUse;

    private void Start()
    {
        isRed = false;
        isBlue = false;

        foreach(Button button in PathButtons)
        {
            button.interactable = false;
        }

        DeployButton.interactable = false;
    }
    private void OnMouseDown()
    {
        print("Click!");
        anEvent.Invoke();
    }

    public void DeployBlueCar()
    {
        GameObject blueCar = Instantiate(BluePlayer, SpawnPoint.transform.position, Quaternion.identity);
        //blueCar.GetComponent<DeployPlayer>().GetPath();
        blueCar.GetComponent<DeployPlayer>().AssignPath(BluePathList[0].GetComponent<WaypointStorer>().wayPoints);
        GameManager.instance.AddBarFillAmount(1f);
    }

    public void DeployRedCar()
    {
        GameObject redCar = Instantiate(RedPlayer, SpawnPoint.transform.position, Quaternion.identity);
        //redCar.GetComponent<DeployPlayer>().GetPath();
        redCar.GetComponent<DeployPlayer>().AssignPath(RedPathList[0].GetComponent<WaypointStorer>().wayPoints);
        GameManager.instance.AddBarFillAmount(1f);
    }

    public void OnClickBlueCar()
    {
        foreach(Text pathText in PathTexts)
        {
            pathText.color = Color.blue;
        }

        isBlue = true;
        isRed = false;

        foreach (Button button in PathButtons)
        {
            button.interactable = true;
        }
    }

    public void OnClickRedCar()
    {
        foreach (Text pathText in PathTexts)
        {
            pathText.color = Color.red;
        }

        isBlue = false;
        isRed = true;

        foreach (Button button in PathButtons)
        {
            button.interactable = true;
        }
    }

    public void OnClickPath1()
    {
        if(isBlue == true)
        {
            pathToUse = BluePathList[0].GetComponent<WaypointStorer>().wayPoints;
        }
        else if(isRed == true)
        {
            pathToUse = RedPathList[0].GetComponent<WaypointStorer>().wayPoints;
        }

        DeployButton.interactable = true;
    }

    public void OnClickPath2()
    {
        if (isBlue == true)
        {
            pathToUse = BluePathList[1].GetComponent<WaypointStorer>().wayPoints;
        }
        else if (isRed == true)
        {
            pathToUse = RedPathList[1].GetComponent<WaypointStorer>().wayPoints;
        }

        DeployButton.interactable = true;
    }

    public void OnClickDeploy()
    {
        if (isBlue == true)
        {
            GameObject blueCar = Instantiate(BluePlayer, SpawnPoint.transform.position, Quaternion.identity);
            blueCar.GetComponent<DeployPlayer>().AssignPath(pathToUse);
            GameManager.instance.AddBarFillAmount(1f);
        }
        else if (isRed == true)
        {
            GameObject redCar = Instantiate(RedPlayer, SpawnPoint.transform.position, Quaternion.identity);
            redCar.GetComponent<DeployPlayer>().AssignPath(pathToUse);
            GameManager.instance.AddBarFillAmount(1f);
        }
    }

    public void OnDeploy()
    {
        foreach (Button button in PathButtons)
        {
            button.interactable = false;
        }
        DeployButton.interactable = false;
    }
}
