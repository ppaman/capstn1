﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    float timer = 8f;
    public GameObject[] Players;
    public bool isOnSpawn = false;
    void Start()
    {
        Instantiate(Players[Random.Range(0, Players.Length)], transform.position, Quaternion.identity);
    }


    void Update()
    {
        timer -= Time.deltaTime;
        if(timer<=0)
        {
            if (isOnSpawn == false)
            {
                Instantiate(Players[Random.Range(0, Players.Length)], transform.position, Quaternion.identity);
                timer = 8f;
            }

            else if (isOnSpawn == true)
            {
                timer = 8f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BluePlayer" || collision.gameObject.tag == "RedPlayer")
        {
            isOnSpawn = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BluePlayer" || collision.gameObject.tag == "RedPlayer")
        {
            isOnSpawn = false;
        }
    }
}
