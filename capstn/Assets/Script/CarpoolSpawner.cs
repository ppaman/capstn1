﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarpoolSpawner : MonoBehaviour
{
    public GameObject redPoint;
    public GameObject bluePoint;
    public int blueCount = 0;
    public int redCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.tag == "Blue" && blueCount < 1)
        {
            SpawnBlue();
        }

        if (gameObject.tag == "Red" && redCount < 1)
        {
            SpawnRed();
        }
    }

    void SpawnRed()
    {
        Instantiate(redPoint, transform.position, Quaternion.identity);
        redCount++;
    }

    void SpawnBlue()
    {
        Instantiate(bluePoint, transform.position, Quaternion.identity);
        blueCount++;
    }
}
