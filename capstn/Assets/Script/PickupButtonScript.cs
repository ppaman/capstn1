﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class PickupButtonScript : MonoBehaviour
{
    [SerializeField] UnityEvent anEvent;
    public DeployPlayer Parent;
    public CarAI AiParent;

    public GameObject MiniGame;
    public AudioSource PickupAudio;
    public AudioSource DropoffAudio;
    private void Start()
    {
        Physics.queriesHitTriggers = true;

        PickupAudio = GameObject.Find("PickupAudio").GetComponent<AudioSource>();
        DropoffAudio = GameObject.Find("DropoffAudio").GetComponent<AudioSource>();
    }
    private void OnMouseDown()
    {
        print("Click!");
        anEvent.Invoke();
    }

    public void PickupPassenger()
    {
        PickupAudio.Play(0);
        Parent.OnPickupPassenger();
    }

    public void Destroy()
    {
        Destroy(this.gameObject);
    }

    public void OnHazardClick()
    {
        SpawnMiniGame();
    }

    void SpawnMiniGame()
    {
        GameManager.instance.AIWithMiniGame = AiParent;
        GameObject miniGame = Instantiate(MiniGame, transform.position, Quaternion.identity);
        Canvas canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        miniGame.transform.SetParent(canvas.transform, false);
    }

    public void DropoffPassenger()
    {
        DropoffAudio.Play(0);
        Parent.OnDropoffPassenger();
    }
}
