﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuButtonHandler : MonoBehaviour
{
    public AudioSource audio;
    public void LevelSelect()
    {
        SceneManager.LoadScene("Level Select");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Pause()
    {
        if(audio != null)
        {
            audio.Pause();
        }
        Time.timeScale = 0;
    }

    public void Resume()
    {
        if (audio != null)
        {
            audio.UnPause();
        }
        Time.timeScale = 1;
    }

    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    public void Menu()
    {
        SceneManager.LoadScene("main Menu");
    }
  
}
