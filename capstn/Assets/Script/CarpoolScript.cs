﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarpoolScript : MonoBehaviour
{
    Player player;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "BluePlayer" || collision.gameObject.tag == "RedPlayer")
        {
            Destroy(gameObject);
            player = collision.gameObject.GetComponent<Player>();
            player.AddPassenger();

            player.moveSpeed = 0f;
        }
    }
}
