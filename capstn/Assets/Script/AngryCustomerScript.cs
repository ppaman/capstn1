﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngryCustomerScript : MonoBehaviour
{
    public GameObject[] waypoints;

    [SerializeField]
    float moveSpeed = 2f;

    [SerializeField]
    GameObject offsetLeft;

    [SerializeField]
    GameObject offsetRight;

    int waypointIndex = 0;
    float startingSpeed;

    public float raycastMaxDistance = 1f;

    public bool isAngry;

    RaycastHit2D hit;
    RaycastHit2D hitLeft;
    RaycastHit2D hitRight;

    private void Awake()
    {
        //Physics2D.autoSyncTransforms = true;
    }

    void Start()
    {
        Physics2D.queriesStartInColliders = false;
        isAngry = true;
        startingSpeed = moveSpeed;
    }


    void Update()
    {
        
        if (waypoints.Length > 0)
        {
            Move();
        }

        if (hit.collider == null || hit.collider.tag == "GreenLight" || hitLeft.collider == null || hitLeft.collider.tag == "GreenLight" || hitRight.collider == null || hitRight.collider.tag == "GreenLight")
        {
            moveSpeed = startingSpeed;
        }
        else if (hit.collider.tag == "car" || hit.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitLeft.collider.tag == "car" || hitLeft.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitRight.collider.tag == "car" || hitRight.collider.tag == "RedLight" || hit.collider.tag == "Player")
        {
            moveSpeed = 0f;
        }
    }

    private void FixedUpdate()
    {
        
    }

    private void LateUpdate()
    {
        CheckCollision();
    }

    void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);
        if (transform.position == waypoints[waypointIndex].transform.position)
        {
            if (waypointIndex < waypoints.Length - 1)
                waypointIndex += 1;
        }

        Vector3 vectorToTarget = waypoints[waypointIndex].transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 10f);

        Physics2D.SyncTransforms();
    }


    void CheckCollision()
    {
        int carLayerMask = 1 << LayerMask.NameToLayer("Car");
        int stopLightLayerMask = 1 << LayerMask.NameToLayer("Stoplight");

        hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector2.right), raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.red);

        hitLeft = Physics2D.Raycast(offsetLeft.transform.position, transform.TransformDirection(Vector2.right), raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(offsetLeft.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);

        hitRight = Physics2D.Raycast(offsetRight.transform.position, transform.TransformDirection(Vector2.right), raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(offsetRight.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);

       
    }
}
