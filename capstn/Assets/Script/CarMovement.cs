﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{

    [SerializeField]
    GameObject[] waypoints;

    [SerializeField]
    float moveSpeed = 2f;

    [SerializeField]
    GameObject offsetLeft;

    [SerializeField]
    GameObject offsetRight;

    int waypointIndex = 0;
    int randomWP;
    float startingSpeed;

    GameObject[] PathList;
    string pathTag;
    public float raycastMaxDistance = 1f;
    GameObject rightHandSideSpawn;
    GameObject leftHandSideSpawn;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.queriesStartInColliders = false;
        leftHandSideSpawn = GameObject.FindGameObjectWithTag("LeftHandSpawner");
        rightHandSideSpawn = GameObject.FindGameObjectWithTag("RightHandSpawner");

        if (this.transform.position == rightHandSideSpawn.transform.position)
        {
            PathList = GameObject.FindGameObjectsWithTag("Path");
        }
        else if (this.transform.position == leftHandSideSpawn.transform.position)
        {
            PathList = GameObject.FindGameObjectsWithTag("ReversePath");
        }

        randomWP = Random.Range(0, PathList.Length);
        waypoints = PathList[randomWP].GetComponent<WaypointStorer>().wayPoints;

        startingSpeed = moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void FixedUpdate()
    {
        CheckCollision();
    }

    void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, waypoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);
        if (transform.position == waypoints[waypointIndex].transform.position)
        {
            if (waypointIndex < waypoints.Length - 1)
                waypointIndex += 1;
        }

        Vector3 vectorToTarget = waypoints[waypointIndex].transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 10f);
    }


    void CheckCollision()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right, raycastMaxDistance);
        Debug.DrawRay(transform.position, transform.right * raycastMaxDistance, Color.red);

        RaycastHit2D hitLeft = Physics2D.Raycast(offsetLeft.transform.position, transform.right, raycastMaxDistance);
        Debug.DrawRay(offsetLeft.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);

        RaycastHit2D hitRight = Physics2D.Raycast(offsetRight.transform.position, transform.right, raycastMaxDistance);
        Debug.DrawRay(offsetRight.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);

        if (hit.collider == null || hit.collider.tag == "GreenLight" || hitLeft.collider == null || hitLeft.collider.tag == "GreenLight" || hitRight.collider == null || hitRight.collider.tag == "GreenLight")
        {
            moveSpeed = startingSpeed;
        }
        else if (hit.collider.tag == "car" || hit.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitLeft.collider.tag == "car" || hitLeft.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitRight.collider.tag == "car" || hitRight.collider.tag == "RedLight" || hit.collider.tag == "Player")
        {
            moveSpeed = 0f;
        }
    }
}

