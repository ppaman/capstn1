﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameSpawner : MonoBehaviour
{
    public GameObject[] minigameCanvas;
    public GameObject[] minigameScripts;

    public int spawnRNG;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnMinigame()
    {
         int random = Random.Range(0, minigameCanvas.Length);
         Instantiate(minigameCanvas[random], this.transform.position, Quaternion.identity);
         Instantiate(minigameScripts[random], this.transform.position, Quaternion.identity);
    }

    public void SpawnRNG()
    {
        spawnRNG = Random.Range(0,100);
        Debug.Log(spawnRNG);        
        if(spawnRNG <= 20)
        {
            SpawnMinigame();
        }
    }
}
