﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimatePoints : MonoBehaviour
{
    public float speed;
    public float despawnTimer;

    public Text PointsText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PointsAnimation();
    }

    void PointsAnimation()
    {
        transform.Translate(Vector2.up * speed * Time.deltaTime);
        Destroy(this.gameObject, 0.5f);
    }

    public void SetText(string text)
    {
        if (PointsText != null)
        {
            PointsText.text = text;
        }   
    }

    public void SetColor(Color color)
    {
        if(PointsText!=null)
        {
            PointsText.color = color;
        }
    }
}
