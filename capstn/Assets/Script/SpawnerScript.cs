﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    public GameObject[] PathList;
    public GameObject[] Cars;
    public bool isOnSpawn = false;
    [SerializeField] float SpawnTimer;

    public float MaxSpawnTimer;
    public float MinSpawnTimer;

    [SerializeField]
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        SpawnTimer = MaxSpawnTimer;
        timer = SpawnTimer;
        SpawnCar();   
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (isOnSpawn == false)
            {
                SpawnCar();
                timer = SpawnTimer;
            }

            else if (isOnSpawn == true)
            {
                timer = SpawnTimer;
            }
        }
    }

    void SpawnCar()
    {
        GameObject car =  Instantiate(Cars[Random.Range(0, Cars.Length)], transform.position, Quaternion.identity);
        int randomWP = Random.Range(0, PathList.Length);
        car.GetComponent<CarAI>().waypoints = PathList[randomWP].GetComponent<WaypointStorer>().wayPoints;
        GameManager.instance.AddBarFillAmount(1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "car")
        {
            isOnSpawn = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "car")
        {
            isOnSpawn = false;
        }
    }

    public void IncreaseTraffic()
    {
        if(SpawnTimer > MinSpawnTimer)
        {
            SpawnTimer -= 0.5f;
            timer = SpawnTimer;
            Debug.Log(SpawnTimer);
        }
    }

    public void DecreaseTraffic()
    {
        if (SpawnTimer < MaxSpawnTimer)
        {
            SpawnTimer += 0.5f;
            timer = SpawnTimer;
            Debug.Log(SpawnTimer);
        }
    }
}
