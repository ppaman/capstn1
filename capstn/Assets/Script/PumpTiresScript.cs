﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PumpTiresScript : MonoBehaviour
{
    public Image tireImage;
    private float maxPump;
    public float clickPump;
    public Text statusText;
    public Button continueButton;

    private void Start()
    {
        continueButton.gameObject.SetActive(false);
        statusText.enabled = true;
        tireImage.fillAmount = 0;
    }

    private void Update()
    {
        if(tireImage.fillAmount<1f)
        {
            if(tireImage.fillAmount <= 0)
            {
                tireImage.fillAmount = 0;
            }
            tireImage.fillAmount -= (0.5f / 100f);
        }

        else if(tireImage.fillAmount >=1f)
        {
            tireImage.fillAmount = 1f;
            continueButton.gameObject.SetActive(true);
            statusText.enabled = false;
        }
    }
    public void PumpTire()
    {
        tireImage.fillAmount += (clickPump / 100f);
    }

    public void OnMinigameEnd()
    {
        GameManager.instance.EndMiniGame();
        Destroy(this.gameObject);
    }

}
