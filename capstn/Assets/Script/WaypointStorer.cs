﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointStorer : MonoBehaviour
{
    public GameObject[] wayPoints;
    public LineRenderer lineRenderer;

    private float counter;
    private float lineDrawSpeed = 6f;

    private void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if(lineRenderer != null)
        {
            lineRenderer.positionCount = wayPoints.Length;

            for (int i = 0; i < wayPoints.Length; i++)
            {
                lineRenderer.SetPosition(i, wayPoints[i].transform.position);
            }
        }
        
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < wayPoints.Length-1; i++)
        {
            Gizmos.DrawLine(wayPoints[i].transform.position, wayPoints[i + 1].transform.position);
        }
    }

}
