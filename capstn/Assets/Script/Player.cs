﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    public float raycastMaxDistance = 1f;

    public float moveSpeed = 2f;
    public int passenger = 0;
    public GameObject[] curPaths;

    [SerializeField]  GameObject[] Path1;
    [SerializeField]  GameObject[] Path2;
    [SerializeField] TextMeshProUGUI PickupText;
    private int waypointIndex = 0;

    private bool isPickingUp;

    

    private void Start()
    {
        Physics2D.queriesStartInColliders = false;
        isPickingUp = false;
    }
    void Update()
    {
        if(curPaths.Length>0)
        {
            Move();
        }
        if(isPickingUp==false)
        {
            PickupText.enabled = false;
            CheckCollision();
        }
        else if(isPickingUp==true)
        {
            PickupText.enabled = true;
        }
    }

    void Move()
    {
        Quaternion rotation = Quaternion.LookRotation
        (curPaths[waypointIndex].transform.position - transform.position, Vector2.left);
        //transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
        transform.position = Vector2.MoveTowards(transform.position, curPaths[waypointIndex].transform.position, moveSpeed * Time.deltaTime);
        if (transform.position == curPaths[waypointIndex].transform.position)
        {
            if (waypointIndex < curPaths.Length - 1)
                waypointIndex += 1;
        }

        Vector3 vectorToTarget = curPaths[waypointIndex].transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 10f);
    }


    void CheckCollision()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position,transform.right,raycastMaxDistance);
        Debug.DrawRay(transform.position, transform.right*raycastMaxDistance, Color.red);
     
        if (hit.collider == null || hit.collider.tag == "GreenLight")
        {
            moveSpeed = 2f;
        }
        else if (hit.collider != null)
        {
            if (hit.collider.tag == "car" || hit.collider.tag == "RedLight"  || hit.collider.tag == "BluePlayer" || hit.collider.tag == "RedPlayer")
            {
                moveSpeed = 0f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Red"||collision.gameObject.tag=="Blue")
        {
            isPickingUp = true;
            moveSpeed = 0f;
            StartCoroutine(PickUpWaitingTime());
        }
    }
    public void ClearWaypoint()
    {
        curPaths = new GameObject[0];
        waypointIndex = 0;
    }

    public void AddPassenger()
    {
        passenger++;
    }

    public void ResetPassenger()
    {
        passenger = 0;
    }

    public void GoPath1()
    {
        curPaths = Path1;
    }

    public void GoPath2()
    {
        curPaths = Path2;
    }

    public IEnumerator PickUpWaitingTime()
    {
        yield return new WaitForSeconds(2.0f);
        isPickingUp = false;
    }

}
