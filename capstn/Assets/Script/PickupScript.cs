﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupScript : MonoBehaviour
{
    public bool isRedPickup;
    public bool isBluePickup;
    public bool isGreenPickup;

    public float cooldownTime;

    public float angryTimer;
    [SerializeField] float timer;

    public GameObject carSpawnPos;
    public GameObject carToSpawn;
    public GameObject angryImageSpawnPos;
    public Text CooldownText;
    public Image CooldownImage;
    public GameObject[] angryImages;

    public GameObject[] PathList;

    private Vector3 StartingScale;

    private void Start()
    {
        timer = angryTimer;
        StartingScale = this.transform.localScale;
        CooldownImage.fillAmount = 1;
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        CooldownImage.fillAmount = (timer / 100) * (100 / (angryTimer));
        if (timer <= 0)
        {
            SpawnAngryCar();
            StartCooldown(0);
            timer = angryTimer;
        }
        //else if (timer <= 10f)
        //{
        //    CooldownImage.color = Color.red;
        //}
        CooldownText.text = (Mathf.RoundToInt(timer)).ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (isRedPickup == true && collision.gameObject.GetComponent<DeployPlayer>().isRed == true || isBluePickup == true && collision.gameObject.GetComponent<DeployPlayer>().isBlue == true || isGreenPickup == true && collision.gameObject.GetComponent<DeployPlayer>().isGreen == true)
            {
                CooldownImage.fillAmount = 1;
                //CooldownImage.color = Color.gray;
                timer = angryTimer;
                collision.gameObject.GetComponent<DeployPlayer>().PickupPassengerSequence();
                collision.gameObject.GetComponent<DeployPlayer>().PickupPin = this;
            }
        }
    }

    public void StartCooldown(float waitTime)
    {
        StartCoroutine(Cooldown(waitTime));
    }
    IEnumerator Cooldown(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        float counter = cooldownTime;
        while(counter>0)
        {
            this.transform.localScale = new Vector3(0, 0, 0);
            yield return new WaitForSeconds(1);
            counter--;
        }
        this.transform.localScale = StartingScale;
        CooldownImage.fillAmount = 1;
        timer = angryTimer;
    }

    void SpawnAngryCar()
    {
        Instantiate(angryImages[Random.Range(0, angryImages.Length - 1)], angryImageSpawnPos.transform.position, Quaternion.identity);
        GameObject car = Instantiate(carToSpawn, carSpawnPos.transform.position, Quaternion.identity);
        int randomWP = Random.Range(0, PathList.Length);
        car.GetComponent<CarAI>().waypoints = PathList[randomWP].GetComponent<WaypointStorer>().wayPoints;
        GameManager.instance.AddBarFillAmount(1f);
    }
}
