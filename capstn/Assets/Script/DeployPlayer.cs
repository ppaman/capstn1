﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeployPlayer : MonoBehaviour
{
    public float raycastMaxDistance;
    public float moveSpeed;
    public float waitTime;
    public int passenger = 0;
    public int spawnChance;

    public GameObject[] curPaths;
    public bool isBlue;
    public bool isRed;
    public bool isGreen;
    public bool isPickingUp;

    public int PointsMinimum;
    public int PointsMaximum;

    public GameObject PointsPrefab;
    public GameObject Minigame;
    public GameObject CustomerPickupPin;
    public PickupScript PickupPin;
    public GameObject DropoffPin;

    [SerializeField] GameObject[] AssignedPath;
    private int waypointIndex = 0;
    private float startingSpeed;
    private Canvas canvas;
 
    [SerializeField]
    GameObject offsetLeft;
    [SerializeField]
    GameObject offsetRight;
    [SerializeField]
    GameObject pinSpawn;

    RaycastHit2D hit;
    RaycastHit2D hitLeft;
    RaycastHit2D hitRight;

    private void Awake()
    {
        //Physics2D.autoSyncTransforms = true;
    }

    private void Start()
    {
        Physics2D.queriesStartInColliders = false;
        isPickingUp = false;

        startingSpeed = moveSpeed;

        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    }
    void Update()
    {
        if (curPaths.Length > 0)
        {
            Move();
        }

        if (isPickingUp == false)
        {
            if (hit.collider == null || hit.collider.tag == "GreenLight" || hitLeft.collider == null || hitLeft.collider.tag == "GreenLight" || hitRight.collider == null || hitRight.collider.tag == "GreenLight")
            {
                moveSpeed = startingSpeed;
            }
            else if (hit.collider.tag == "car" || hit.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitLeft.collider.tag == "car" || hitLeft.collider.tag == "RedLight" || hit.collider.tag == "Player" || hitRight.collider.tag == "car" || hitRight.collider.tag == "RedLight" || hit.collider.tag == "Player")
            {
                moveSpeed = 0f;
            }
        }
    }

    private void FixedUpdate()
    {
        
    }

    private void LateUpdate()
    {
        CheckCollision();
    }

    void Move()
    {
        Quaternion rotation = Quaternion.LookRotation
        (curPaths[waypointIndex].transform.position - transform.position, Vector2.left);
        //transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
        transform.position = Vector2.MoveTowards(transform.position, curPaths[waypointIndex].transform.position, moveSpeed * Time.deltaTime);
        if (transform.position == curPaths[waypointIndex].transform.position)
        {
            if (waypointIndex < curPaths.Length - 1)
            {
                waypointIndex += 1;
            }
            else if(waypointIndex >= curPaths.Length -1)
            {
                waypointIndex = 0;
            }
                
        }

        Vector3 vectorToTarget = curPaths[waypointIndex].transform.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 10f);

        Physics2D.SyncTransforms();
    }


    void CheckCollision()
    {
        int carLayerMask = 1<< LayerMask.NameToLayer("Car");
        int stopLightLayerMask = 1 << LayerMask.NameToLayer("Stoplight");
        hit = Physics2D.Raycast(transform.position, transform.right, raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(transform.position, transform.right * raycastMaxDistance, Color.red);

        hitLeft = Physics2D.Raycast(offsetLeft.transform.position, transform.right, raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(offsetLeft.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);

        hitRight = Physics2D.Raycast(offsetRight.transform.position, transform.right, raycastMaxDistance, carLayerMask | stopLightLayerMask);
        Debug.DrawRay(offsetRight.transform.position, transform.TransformDirection(Vector2.right) * raycastMaxDistance, Color.blue);  
    }

    public void AddPassenger()
    {
        passenger++;
    }

    public void ResetPassenger()
    {
        passenger = 0;
    }

    public void GetPath()
    {
        curPaths = AssignedPath;
    }

    public void AssignPath(GameObject[] Path)
    {
        curPaths = Path;
    }

    public void SpawnPointPrefab()
    {
        int pointToGive = Random.Range(PointsMinimum, PointsMaximum);

        GameObject point = Instantiate(PointsPrefab, this.transform.position, Quaternion.identity);
        point.GetComponent<AnimatePoints>().SetColor(Color.white);
        point.GetComponent<AnimatePoints>().SetText("+" + pointToGive.ToString());
        GameManager.instance.AddScore(pointToGive);
    }

    public void PickupPassengerSequence()
    {
        isPickingUp = true;
        moveSpeed = 0f;
        SpawnPickupPin();
    }

    public void OnPickupPassenger()
    {
        isPickingUp = false;
        SpawnPointPrefab();
        if(PickupPin!=null)
        {
            PickupPin.StartCooldown(0.5f);
        }
        
        //CustomerPickupPin.gameObject.SetActive(false);
    }

    public void DropoffSequence()
    {
        isPickingUp = true;
        moveSpeed = 0f;
        SpawnDropoffPin();
    }

    public void OnDropoffPassenger()
    {
        isPickingUp = false;
    }

    public void SpawnMinigame()
    {
        GameManager.instance.SetCarWithMinigame(this);
        GameObject miniGame = Instantiate(Minigame, this.transform.position, Quaternion.identity);
        miniGame.transform.SetParent(canvas.transform, false);
    }

    void SpawnPickupPin()
    {
        GameObject pin = Instantiate(CustomerPickupPin, pinSpawn.transform.position, Quaternion.identity);
        pin.GetComponent<PickupButtonScript>().Parent = this;
    }

    void SpawnDropoffPin()
    {
        GameObject dropOffPin = Instantiate(DropoffPin, pinSpawn.transform.position, Quaternion.identity);
        dropOffPin.GetComponent<PickupButtonScript>().Parent = this;
    }
}
