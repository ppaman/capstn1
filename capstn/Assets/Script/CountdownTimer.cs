﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 120f;

    [SerializeField] Text countdownText;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        TimeSpan ts = TimeSpan.FromSeconds(currentTime);
        string displayTimer = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
        countdownText.text = displayTimer;

        if(currentTime<=10)
        {
            countdownText.color = Color.red;
        }

        if (currentTime <= 0)
        {
            Time.timeScale = 0;
            currentTime = 0;
            SceneManager.LoadScene("End Scene");
        }
    }
}
