﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class Tire : MonoBehaviour
{
    public Canvas canvas;
    public Slider slider;
    public Button button;
    float currentPumpValue;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.FindGameObjectWithTag("TireCanvas").GetComponent<Canvas>();
        slider = GameObject.FindGameObjectWithTag("TireCanvas").GetComponentInChildren<Slider>();
        button = GameObject.FindGameObjectWithTag("TireCanvas").GetComponentInChildren<Button>();
        currentPumpValue = slider.minValue;

        button.onClick.AddListener(AddPump);
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = currentPumpValue;

        if(currentPumpValue >= slider.maxValue)
        {
            OnComplete();
        }
    }

    public void AddPump()
    {
        currentPumpValue++;
    }

    void OnComplete()
    {
        Destroy(canvas.gameObject);
        Destroy(gameObject);
    }
}
